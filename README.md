# Website Clones Blocker

This repository contains lists with domains of websites I don't want to see in search result nor want them to load anything in my browser. The lists provided in here are following the AdBlock format and should be digestible by most ad-blocker browser-extensions.

The lists do contain the same set of domains. The difference is in the format, as [website-clones.list](block-website-clones.list) is used to block any requests to those websites.

The other both lists [remove-website-clones-from-google-results.list](remove-website-clones-from-google-results.list) and  [remove-website-clones-from-duckduckgo-results.list](remove-website-clones-from-duckduckgo-results.list) are used to hide those websites from the respective search results.
